package object

func NewEnclosedScope(outer *Scope) *Scope {
	env := NewScope()
	env.outer = outer
	return env
}

func NewScope() *Scope {
	s := make(map[string]Object)
	return &Scope{store: s, outer: nil}
}

type Scope struct {
	store map[string]Object
	outer *Scope
}

func (e *Scope) LookUp(name string) (Object, bool) {
	obj, ok := e.store[name]
	if !ok && e.outer != nil {
		obj, ok = e.outer.LookUp(name)
	}
	return obj, ok
}

func (e *Scope) Add(name string, val Object) Object {
	e.store[name] = val
	return val
}
