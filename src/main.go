package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"runtime"

	eval "bitbucket.org/pistachio05/gointrep/src/backend/eval"
	"bitbucket.org/pistachio05/gointrep/src/frontend/lexer"
	"bitbucket.org/pistachio05/gointrep/src/frontend/parser"
	"bitbucket.org/pistachio05/gointrep/src/midend/object"
)

const PROMPT = ">>> "
const EXIT = "Terminating The Interpreter...\n Thank you for using Sucks!"

func start(in io.Reader, out io.Writer) {
	scanner := bufio.NewScanner(in)
	scope := object.NewScope()

	for {
		fmt.Printf(PROMPT)
		scanned := scanner.Scan()
		if !scanned {
			return
		}

		line := scanner.Text()
		if line == "exit" {
			fmt.Printf(EXIT)
			os.Exit(0)
		}
		l := lexer.New(line)
		p := parser.New(l)

		program := p.ParseProgram()

		if len(p.Errors()) != 0 {
			io.WriteString(out, "(ノ﹏ヽ)\n parser errors:\n")
			for _, msg := range p.Errors() {
				io.WriteString(out, "\t"+msg+"\n")
			}
			continue
		}

		evaluated := eval.Eval(program, scope)
		if evaluated != nil {
			io.WriteString(out, evaluated.Inspect())
			io.WriteString(out, "\n")
		}
	}
}

func main() {
	fmt.Println("Welcome to Sucks Intrepreter. Running on " + runtime.GOOS + "-" + runtime.GOARCH)
	start(os.Stdin, os.Stdout)
}
